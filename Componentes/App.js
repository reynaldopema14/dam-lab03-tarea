import React, {Component} from "react";
import { StyleSheet, TouchableOpacity, Text, View, Image, TextInput } from "react-native";
import AgeValidator from "./components/ageValidator/AgeValidator";
import MyList from "./components/myList/MyList"

export default class App extends Component{

  render(){
    return (
      <View style={styles.container}>
        <View style={styles.text}>
          <Text>Ingrese su edad</Text>
        </View>
        <AgeValidator/>

        <MyList/>
      </View> 
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    paddingHorizontal: 10,
    backgroundColor: 'coral',
  },

  text: {
    alignItems: 'center',
    padding: 5,
  },
});