import React, {Component} from "react";
import { View, FlatList, StyleSheet, Text, TouchableOpacity, Image }
from 'react-native';

export default class App extends Component{
   constructor(props) {
       super(props)
       this.state = {
           lista: [
               {
                   nombre: 'Ramon Valdez',
                   img: 'https://www.latercera.com/resizer/tqWFmYb_Bj1DZNZedT2Ae9xHQeQ=/900x600/smart/arc-anglerfish-arc2-prod-copesa.s3.amazonaws.com/public/PYEGWJNRBJCP7AV5IESSJCQV4Q.jpg'
               },
               {
                    nombre: 'Ruben Aguirre',
                    img: 'http://cde.3.elcomercio.pe/ima/0/1/4/1/3/1413161.jpg'
                },
                {
                    nombre: 'Florinda Meza',
                    img: 'https://www.mdzol.com/u/fotografias/m/2021/2/16/f1456x819-1019239_1189609_5050.jpg'
                },
                {
                    nombre: 'Carlos Villagran',
                    img: 'https://pbs.twimg.com/profile_images/1319139219/Kiko3_1__400x400.jpg'
                },
                {
                    nombre: 'Maria Antonieta de las Nieves',
                    img: 'https://elcomercio.pe/resizer/pggn6SsN3ozg5AdOzyGB3QzrkZU=/580x330/smart/filters:format(jpeg):quality(75)/cloudfront-us-east-1.images.arcpublishing.com/elcomercio/WS3E5ULEWZGOLBZZDMUWJADRKE.jpg'
                },
                {
                    nombre: 'Edgar Vivar',
                    img: 'https://peru21.pe/resizer/qYQJ2onpEF-_lx1OC-HiKOqS0yQ=/580x330/smart/filters:format(jpeg):quality(75)/cloudfront-us-east-1.images.arcpublishing.com/elcomercio/PWJAK7XQD5DDPKYRXCAWJ4XE2U.jpg'
                },              
           ]
       }
   }
   renderItem = ({item}) => (
       <TouchableOpacity>
           <View style={styles.itemContainer}>
            <Image
                style={{width: 100, height: 100, borderRadius: 50}}
                source={{uri: item.img}}
            />
               <Text style={styles.itemName}>{item.nombre}</Text>
           </View>
       </TouchableOpacity>
   )
   keyExtractor = (item,index) => index.toString()

   FlatListSeparador = () => {
       return (
           <View style={{height:1, width: '100%', backgroundColor: 'black'}} 
           />

       )
   }

   render(){
       return(
           <View style={styles.container}>
               <FlatList
                    keyExtractor={this.keyExtractor}
                    data={this.state.lista}
                    renderItem={this.renderItem}
                    ItemSeparatorComponent={this.FlatListSeparador}
               />
           </View>
       )
   }
}
const styles = StyleSheet.create({
    container: {
      flex: 1,
      marginTop: 20,
    },
  
    itemContainer:{
        flex: 1,
        flexDirection: 'row',
        marginLeft:20,
        justifyContent: 'flex-start',
        margin:15,
    },
    itemName:{
        marginLeft:20,
        fontSize:20,
    },
    
});
